package juego;

import java.awt.Image;

import javax.sound.sampled.Clip;

import entorno.Entorno;

import entorno.Herramientas;

public class Bola_Fuego {
	private int x;
	private int y;
	private int alto;
	private int ancho;
	private Image boladefuego;
	private Clip disparo;
	
	Bola_Fuego(int x, int y, int alto, int ancho) {
		this.x = x;
		this.y = y;
		this.alto = alto;
		this.ancho = ancho;
		this.boladefuego = Herramientas.cargarImagen("bola.png");
		this.disparo=Herramientas.cargarSonido("disparo.wav");
	}

	public void avanzarDerecha() {
		this.x = this.x + 2;
	}
	
	public void avanzarIzquierda() {
		this.x = this.x - 3;
		this.y = this.y + 1;
	}

	public void dibujar(Entorno e) {
		e.dibujarImagen(boladefuego, this.x, this.y, 0, 0.1);
		this.disparo.start();
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getAlto() {
		return alto;
	}

	public int getAncho() {
		return ancho;
	}
}