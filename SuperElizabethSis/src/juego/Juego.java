package juego;

import java.awt.Color;

import java.awt.Image;

import java.util.LinkedList;

import java.util.Random;

import javax.sound.sampled.Clip;

import entorno.Entorno;

import entorno.Herramientas;

import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {

	// El objeto Entorno que controla el tiempo y otros.
	private Entorno entorno;

	private Image fondo;
	private Image fondoRey;
	private Image gatito;
	private Image suelo;
	private Image sueloRey;

	private Clip musicaFondo;
	private Clip gameOver;
	private Clip musicaRey;

	private LinkedList<Obstaculo> listaObstaculos;
	private LinkedList<Bola_Fuego> listaDisparos;
	private LinkedList<Soldado> listaSoldaditos;
	private LinkedList<Bola_Fuego> listaDisparosRey;

	private Princesa princ;
	private ReyCamir rey;

	private int vidas;
	private int vidasRey;
	private int score;
	private int nivel;

	private VidaExtra vidasExtra[];

	// Variables y m�todos propios de cada grupo

	Juego() {

		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "Super Elizabeth Sis - Grupo 2", 800, 600);

		// Inicializar lo que haga falta para el juego
		this.fondo = Herramientas.cargarImagen("fondo.png");
		this.fondoRey = Herramientas.cargarImagen("fondoBatallaRey.png");
		this.suelo = Herramientas.cargarImagen("suelo.png");
		this.sueloRey = Herramientas.cargarImagen("lavaFloor.png");
		this.gatito = Herramientas.cargarImagen("gatito.png");
		this.musicaFondo = Herramientas.cargarSonido("MusicaMario.wav");
		this.gameOver = Herramientas.cargarSonido("gameOver.wav");
		this.musicaRey = Herramientas.cargarSonido("musicaFondoRey.wav");

		this.listaObstaculos = new LinkedList<Obstaculo>();
		this.listaObstaculos.add(new Obstaculo(800, 525, 50, 45, 0.5));
		this.listaObstaculos.add(new Obstaculo(950, 475, 150, 45, 0.5));
		this.listaObstaculos.add(new Obstaculo(1120, 510, 80, 45, 0.5));
		this.listaObstaculos.add(new Obstaculo(1570, 495, 110, 45, 0.5));

		this.princ = new Princesa(200, 500, 60, 100);

		this.listaSoldaditos = new LinkedList<Soldado>();

		this.listaSoldaditos.add(new Soldado(900, 500, 60, 100, 1));
		this.listaSoldaditos.add(new Soldado(1080, 500, 60, 100, 1));
		this.listaSoldaditos.add(new Soldado(1220, 500, 60, 100, 1));
		this.listaSoldaditos.add(new Soldado(1470, 500, 60, 100, 1));

		this.vidas = 3;
		this.score = 0;
		this.nivel = 1;

		this.listaDisparos = new LinkedList<Bola_Fuego>();
		
		this.listaDisparosRey = new LinkedList<Bola_Fuego>();

		this.vidasExtra = new VidaExtra[5];
		this.vidasExtra[0] = new VidaExtra(1500);
		this.vidasExtra[1] = new VidaExtra(3000);
		this.vidasExtra[2] = new VidaExtra(4000);
		this.vidasExtra[3] = new VidaExtra(5000);
		this.vidasExtra[4] = new VidaExtra(6000);

		this.rey = null;

		this.vidasRey = 50;

		// Inicia el juego!
		this.entorno.iniciar();
	}

	/**
	 * Durante el juego, el m�todo tick() ser ejecutado en cada instante y por lo
	 * tanto es el m�todo m�s importante de esta clase. Aqu� se debe actualizar el
	 * estado interno del juego para simular el paso del tiempo (ver el enunciado
	 * del TP para mayor detalle).
	 */
	public void tick() {

		// Procesamiento de un instante de tiempo

		// Juego

		if (this.vidas > 0) {
			if (this.score <= 150) {
				this.musicaFondo.loop(1);
			}

			this.entorno.dibujarImagen(fondo, 400, 300, 0, 1.05);

			this.entorno.cambiarFont("Consolas", 30, Color.yellow);
			this.entorno.escribirTexto("LIVES " + vidas, 50, 50);

			this.entorno.cambiarFont("Consolas", 30, Color.YELLOW);
			this.entorno.escribirTexto("SCORE " + score, 620, 50);

			this.entorno.cambiarFont("Consolas", 30, Color.yellow);
			this.entorno.escribirTexto("LVL " + nivel, 350, 50);

			this.entorno.dibujarImagen(suelo, 400, 590, 0, 1.03);

			// VidaExtra

			for (int i = 0; i < this.vidasExtra.length; i++) {
				this.vidasExtra[i].dibujar(this.entorno);
				this.vidasExtra[i].moverIzquierda();

				if (this.vidasExtra[i].getX() < -30) {
					this.vidasExtra[i].desaparecer();
				}
			}
			if (colisionPrincVidaExtra()) {
				this.vidas = this.vidas + 1;
			}

			// ReyCamir

			if (this.score >= 150 && this.vidasRey > 1) {
				crearRey();
				this.nivel = 4;
				this.musicaFondo.stop();
				this.musicaRey.loop(1);
				this.entorno.dibujarImagen(fondoRey, 405, 300, 0, 1.03);
				this.entorno.dibujarImagen(sueloRey, 400, 650, 0, 1.03);
				this.rey.dibujar(this.entorno);

				this.entorno.cambiarFont("Consolas", 30, Color.yellow);
				this.entorno.escribirTexto("VIDAS REY " + vidasRey, 350, 150);

				this.entorno.cambiarFont("Consolas", 30, Color.YELLOW);
				this.entorno.escribirTexto("SCORE " + score, 620, 50);
				
				this.entorno.cambiarFont("Consolas", 30, Color.yellow);
				this.entorno.escribirTexto("LVL " + nivel, 350, 50);

				this.entorno.cambiarFont("Consolas", 30, Color.yellow);
				this.entorno.escribirTexto("LIVES " + vidas, 50, 50);

				for (int i = 0; i < this.vidasExtra.length; i++) {
					this.vidasExtra[i].desaparecer();
				}
				this.listaSoldaditos.removeAll(listaSoldaditos);
				this.listaObstaculos.removeAll(listaObstaculos);
				
				
				if(randomBoolean() == true && this.listaDisparosRey.size() < 1) {
					this.listaDisparosRey.addLast(this.rey.disparar());
				}
				
				for(Bola_Fuego bola : this.listaDisparosRey) {
					bola.dibujar(this.entorno);
					bola.avanzarIzquierda();
					if(colisionDisparoSuelo() || colisionDisparoPrincesa() ) {
						this.listaDisparosRey.removeAll(listaDisparosRey);
					}
				}

			}

			if (this.vidasRey <= 0) {
				this.rey = null;
				this.musicaRey.stop();
				this.listaSoldaditos.removeAll(listaSoldaditos);
				this.listaObstaculos.removeAll(listaObstaculos);
				this.listaDisparos.removeAll(listaDisparos);
				this.listaDisparosRey.removeAll(listaDisparosRey);
				for (int i = 0; i < this.vidasExtra.length; i++) {
					this.vidasExtra[i].desaparecer();
				}
				
				this.entorno.dibujarImagen(gatito, 600, 520, 0, 0.2);
				
				this.entorno.cambiarFont("Consolas", 20, Color.yellow);
				this.entorno.escribirTexto("Reencuentro con Gatito! ", 50, 300);
				if (this.entorno.estaPresionada(this.entorno.TECLA_DERECHA)) {
					this.princ.moverDerecha();

				}
			}

			// FinJuego

			if (colisionPrincesaGatito()) {
				this.entorno.cambiarFont("Consolas", 30, Color.yellow);
				this.entorno.escribirTexto("�Has ganado!", 50, 200);
				this.entorno.escribirTexto("Presiona enter para volver a jugar", 50, 250);
				this.musicaFondo.close();
				
				
				
				if (this.entorno.sePresiono(this.entorno.TECLA_ENTER)) {
					reiniciar();
					this.vidas = 3;
					this.score = 0;
					this.nivel = 1;
					this.musicaFondo.loop(1);

				}
			}

			// Princesa

			this.princ.dibujar(this.entorno);
			if (this.entorno.estaPresionada(this.entorno.TECLA_DERECHA) && this.princ.getX() < 400) {
				this.princ.moverDerecha();
			}
			if (this.entorno.estaPresionada(this.entorno.TECLA_IZQUIERDA) && this.princ.getX() > 30) {
				this.princ.moverIzquierda();
			}

			if (this.entorno.sePresiono(this.entorno.TECLA_ARRIBA) && this.princ.getY() == 500
					&& !this.princ.getSalto()) {
				this.princ.saltoTrue();
				this.princ.sonidoSalto();
			}

			this.princ.saltoPrincesa();

			// Soldado

			LinkedList<Soldado> paraEliminar = new LinkedList<Soldado>();

			for (Soldado sold : this.listaSoldaditos) {
				double b = 1.5;
				double d = 2.00;
				if (this.score >= 50 && this.score < 70) {
					sold.aumentarVelocidad(b);
					this.nivel = 2;
				} else if (this.score >= 70) {
					sold.aumentarVelocidad(d);
					this.nivel = 3;
				}

				sold.dibujar(this.entorno);
				sold.moverIzquierda();

				if (sold.getX() < -30) {
					paraEliminar.add(sold);

				}

				if (this.vidas > 0) {
					if (colisionPrincSold()) {
						this.vidas--;
						this.musicaFondo.close();
						reiniciar();
					}
				}
			}

			this.listaSoldaditos.removeAll(paraEliminar);
			crearSoldado();

			// Bola_Fuego

			if (this.entorno.sePresiono(this.entorno.TECLA_ESPACIO)) {
				this.listaDisparos.addLast(this.princ.disparar());
			}

			LinkedList<Bola_Fuego> paraBorrar = new LinkedList<Bola_Fuego>();

			for (Bola_Fuego b : this.listaDisparos) {
				b.dibujar(this.entorno);
				b.avanzarDerecha();

				if (b.getX() > 820) {
					paraBorrar.add(b);
				}
			}

			for (Bola_Fuego b : this.listaDisparos) {
				for (Soldado sold : this.listaSoldaditos) {
					if (colisionBolaSoldado(b, sold)) {
						this.score += 5;
						sold.sonidoSoldadoMuere();
						paraBorrar.add(b);
						paraEliminar.add(sold);
					}
				}
			}
			if (this.rey != null) {
				if (colisionDisparoPrincesaRey() == true) {

					this.vidasRey--;
					this.score = this.score + 10;
				}
			}
			this.listaSoldaditos.removeAll(paraEliminar);
			this.listaDisparos.removeAll(paraBorrar);
			crearSoldado();

			// Obstaculos

			LinkedList<Obstaculo> paraQuitar = new LinkedList<Obstaculo>();

			for (Obstaculo obs : this.listaObstaculos) {
				double a = 1.00;
				double c = 1.5;
				if (this.score >= 50 && this.score < 70) {
					obs.modificarVelocidad(a);
					this.nivel = 2;
				} else if (this.score >= 70) {
					obs.modificarVelocidad(c);
					this.nivel = 3;
				}
				if (obs.getAlto() == 150) {
					obs.dibujarObs1(this.entorno);
				} else if (obs.getAlto() == 110) {
					obs.dibujarObs2(this.entorno);
				} else if (obs.getAlto() == 50) {
					obs.dibujarObs3(this.entorno);
				} else if (obs.getAlto() == 80) {
					obs.dibujarObs4(this.entorno);
				}
				obs.moverIzquierda();

				if (obs.getX() < -30) {
					paraQuitar.add(obs);
					this.score = this.score + 2;

				}

				if (this.vidas > 0) {
					if (colisionPrincObst()) {
						this.musicaFondo.close();
						reiniciar();
						this.vidas--;
					}
				}
			}
			this.listaObstaculos.removeAll(paraQuitar);
			crearObstaculo();

		} else {
			this.entorno.cambiarFont("Consolas", 40, Color.RED);
			this.entorno.escribirTexto("�GAME OVER!", 250, 300);
			this.entorno.escribirTexto("PRESIONE ENTER", 220, 400);
			this.entorno.cambiarFont("Consolas", 30, Color.YELLOW);
			this.entorno.escribirTexto("FINAL SCORE " + score, 250, 100);
			this.gameOver.start();
		}
		if (this.vidas == 0 && this.entorno.sePresiono(this.entorno.TECLA_ENTER)) {

			reiniciar();
			this.gameOver.stop();
			this.vidas = 3;
			this.score = 0;
			vidasRey = 50;
			this.nivel = 1;
			this.vidasExtra = new VidaExtra[5];
			this.vidasExtra[0] = new VidaExtra(1500);
			this.vidasExtra[1] = new VidaExtra(3000);
			this.vidasExtra[2] = new VidaExtra(4000);
			this.vidasExtra[3] = new VidaExtra(5000);
			this.vidasExtra[4] = new VidaExtra(6000);
			this.musicaFondo.start();
		}
	}

	// Funciones
	
	public boolean colisionDisparoSuelo() {
		for(Bola_Fuego b : this.listaDisparosRey) {
			if(b.getY() + b.getAlto()/2 > 550 ) {
				return true;
			}
		}
		return false;
	}
	
	public boolean randomBoolean() {
		int a = randInt(1, 2);
		if(a == 1) {
			return true;
		}else {
			return false;
		}
		
		
	}
	
	public boolean colisionDisparoPrincesa() {
		for(Bola_Fuego b : this.listaDisparosRey) {
			if(b.getX() - b.getAncho()/2 < this.princ.getX() + this.princ.getAncho()/2 &&
				b.getY() + b.getAlto()/2 > this.princ.getY() - this.princ.getAlto()/2 &&
				b.getX() + b.getAncho()/2 > this.princ.getX() - this.princ.getAncho()/2 &&
				b.getY() - b.getAlto()/2 < this.princ.getY() + this.princ.getAlto()/2) {
				this.vidas --;
				return true;
			}
		}
	return false;
	}

	public boolean colisionPrincVidaExtra() {
		for (int i = 0; i < this.vidasExtra.length; i++) {
			if ((this.vidasExtra[i].getX() - this.vidasExtra[i].getAncho() / 2 < this.princ.getX()
					+ this.princ.getAncho() / 2
					&& this.vidasExtra[i].getY() + this.vidasExtra[i].getAlto() / 2 >= this.princ.getY()
							- this.princ.getAlto() / 2)
					|| (this.vidasExtra[i].getX() + this.vidasExtra[i].getAncho() / 2 < this.princ.getX()
							- this.princ.getAncho() / 2
							&& this.vidasExtra[i].getY() + this.vidasExtra[i].getAlto() / 2 >= this.princ.getY()
									- this.princ.getAlto() / 2)) {
				this.vidasExtra[i].desaparecer();
				this.vidasExtra[i].sonidoVida();
				return true;
			}
		}
		return false;
	}

	public boolean colisionDisparoPrincesaRey() {
		for (Bola_Fuego b : this.listaDisparos) {
			if (b.getX() + b.getAncho() / 2 >= this.rey.getX() - this.rey.getAncho() / 2
					&& b.getY() <= this.rey.getY() + this.rey.getAlto() / 2) {
				this.listaDisparos.remove(b);
				return true;
			}
		}
		return false;
	}

	public boolean colisionPrincesaGatito() {
		if (this.princ.getX() + this.princ.getAncho() >= 600) {
			return true;
		}

		return false;
	}

	public static int randInt(int min, int max) {
		Random rand = new Random();
		int randomNum = rand.nextInt((max - min) + 1) + min;
		return randomNum;
	}

	public static double randDouble(int min, int max) {
		Random rand = new Random();
		double randomNum = rand.nextInt((max - min) + 1) + min;
		return randomNum;
	}

	public boolean colisionPrincSold() {
		for (Soldado sold : this.listaSoldaditos) {
			if (this.princ.getX() + this.princ.getAncho() / 2 > sold.getX() - sold.getAncho() / 2
					&& this.princ.getX() - this.princ.getAncho() / 2 < sold.getX() + sold.getAncho() / 2
					&& this.princ.getY() + this.princ.getAlto() / 2 >= sold.getY() - sold.getAlto() / 2) {
				return true;
			}
		}
		return false;
	}

	public boolean colisionPrincObst() {
		for (Obstaculo obs : this.listaObstaculos) {
			if (this.princ.getX() + this.princ.getAncho() / 2 > obs.getX() - obs.getAncho() / 2
					&& this.princ.getX() - this.princ.getAncho() / 2 < obs.getX() + obs.getAncho() / 2
					&& this.princ.getY() + this.princ.getAlto() / 2 == obs.getY() + obs.getAlto() / 2
					|| this.princ.getX() + this.princ.getAncho() / 2 > obs.getX() - obs.getAncho() / 2
							&& this.princ.getX() - this.princ.getAncho() / 2 < obs.getX() + obs.getAncho() / 2
							&& this.princ.getY() + this.princ.getAlto() / 2 >= obs.getY() + obs.getAlto() / 2) {
				return true;
			}
		}
		return false;
	}

	public boolean colisionBolaSoldado(Bola_Fuego b, Soldado sold) {
		if (b.getX() + b.getAncho() / 2 > sold.getX() - sold.getAncho() / 2
				&& b.getX() - b.getAncho() / 2 < sold.getX() + sold.getAncho() / 2
				&& b.getY() - b.getAlto() / 2 >= sold.getY() - sold.getAlto() / 2 - 30) {

			return true;
		}
		return false;
	}

	public Soldado crearSoldado() {
		int a = randInt(800, 3000);
		try {
			if (a >= this.listaSoldaditos.getLast().getX() + this.listaSoldaditos.getLast().getAncho() / 2
					+ randInt(300, 600)) {
				Soldado s = new Soldado(a, 500, 40, 100, 1);
				this.listaSoldaditos.addLast(s);
			}

		} catch (java.util.NoSuchElementException ex) {
			Soldado s = new Soldado(a, 500, 60, 100, 1);
			this.listaSoldaditos.addLast(s);

		}
		return this.listaSoldaditos.getLast();
	}

	public Obstaculo crearObstaculo() {

		double a = randDouble(800, 1700);
		int d = randInt(1, 4);
		int b = 0;
		if (d == 1) {
			b = 150;
		} else if (d == 2) {
			b = 110;
		} else if (d == 3) {
			b = 80;
		} else {
			b = 50;
		}
		int c = 550 - (b / 2);
		try {
			if (a >= this.listaObstaculos.getLast().getX() + this.listaObstaculos.getLast().getAncho() / 2 + 200) {
				this.listaObstaculos.addLast(new Obstaculo(a, c, b, 40, 0.5));
			}
		} catch (java.util.NoSuchElementException ex) {
			this.listaObstaculos.addLast(new Obstaculo(a, c, b, 40, 0.5));
		}
		return this.listaObstaculos.getLast();
	}

	public void reiniciar() {
		this.fondo = Herramientas.cargarImagen("fondo.png");
		this.suelo = Herramientas.cargarImagen("suelo.png");
		this.sueloRey = Herramientas.cargarImagen("lavaFloor.png");
		this.gatito = Herramientas.cargarImagen("gatito.png");
		this.musicaFondo = Herramientas.cargarSonido("MusicaMario.wav");

		this.listaObstaculos = new LinkedList<Obstaculo>();
		this.listaObstaculos.add(new Obstaculo(800, 525, 50, 45, 0.5));
		this.listaObstaculos.add(new Obstaculo(950, 475, 150, 45, 0.5));
		this.listaObstaculos.add(new Obstaculo(1120, 510, 80, 45, 0.5));
		this.listaObstaculos.add(new Obstaculo(1570, 495, 110, 45, 0.5));

		this.princ = new Princesa(200, 500, 60, 100);

		this.listaSoldaditos = new LinkedList<Soldado>();

		this.listaSoldaditos.add(new Soldado(900, 500, 60, 100, 1));
		this.listaSoldaditos.add(new Soldado(1080, 500, 60, 100, 1));
		this.listaSoldaditos.add(new Soldado(1220, 500, 60, 100, 1));
		this.listaSoldaditos.add(new Soldado(1470, 500, 60, 100, 1));

		this.listaDisparos = new LinkedList<Bola_Fuego>();

		this.vidasExtra = new VidaExtra[5];
		this.vidasExtra[0] = new VidaExtra(1500);
		this.vidasExtra[1] = new VidaExtra(3000);
		this.vidasExtra[2] = new VidaExtra(4000);
		this.vidasExtra[3] = new VidaExtra(5000);
		this.vidasExtra[4] = new VidaExtra(6000);

		this.vidasRey = 50;
	}

	public void crearRey() {
		if (this.score >= 150) {
			this.rey = new ReyCamir(650, 440, 80, 220);
		}

	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	}
}