package juego;

import java.awt.Image;

import javax.sound.sampled.Clip;

import entorno.Entorno;

import entorno.Herramientas;

public class VidaExtra {
	private int x;
	private int y;
	private int ancho;
	private int alto;
	private Image vida;
	private Clip vidaExtra;
	
	VidaExtra(int x){
		this.x = x;
		this.y = 250;
		this.ancho = 10;
		this.alto = 10;
		this.vida = Herramientas.cargarImagen("1up.png");
		this.vidaExtra=Herramientas.cargarSonido("vida.wav");
	}
	
	public void desaparecer() {
		this.x = 1000;
		this.y = -1000;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getAncho() {
		return ancho;
	}

	public int getAlto() {
		return alto;
	}

	public void dibujar(Entorno e) {
		e.dibujarImagen(vida, this.x, this.y, 0, 0.1);
	}
	
	public void sonidoVida() {
		this.vidaExtra.start();
	}
	
	public void moverIzquierda() {
		this.x = this.x - 1;
	}
}