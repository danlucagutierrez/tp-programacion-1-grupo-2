package juego;

import java.awt.Image;
import java.util.Random;

import entorno.Entorno;

import entorno.Herramientas;

public class ReyCamir {
	private int x;
	private int y;
	private int alto;
	private int ancho;
	private Image reyCamir;
	
	
	ReyCamir(int x, int y, int ancho, int alto) {
		this.x = x;
		this.y = y;
		this.alto = alto;
		this.ancho = ancho;
		this.reyCamir = Herramientas.cargarImagen("ReyEnojado.png");
	}
	
	public void dibujar(Entorno e) {
		e.dibujarImagen(reyCamir, this.x + 10, this.y - 35, 0, 0.8);
	}
	
	public Bola_Fuego disparar() {
		Bola_Fuego bola = new Bola_Fuego(this.x, randInt(310,500), 10, 10);
		return bola;
	}
	
	public int randInt(int min, int max) {
		Random rand = new Random();
		int randomNum = rand.nextInt((max - min) + 1) + min;
		return randomNum;
	}

	public void moverIzquierda() {
		this.x --;
	}
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getAlto() {
		return alto;
	}

	public int getAncho() {
		return ancho;
	}
}