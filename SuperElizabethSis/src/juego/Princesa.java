package juego;

import java.awt.Image;

import entorno.Entorno;

import entorno.Herramientas;

public class Princesa {
	private int x;
	private int y;
	private int alto;
	private int ancho;
	private boolean salto;
	private boolean saltoMaximo;
	private Image princesa;


	Princesa(int x, int y, int ancho, int alto) {
		this.x = x;
		this.y = y;
		this.alto = alto;
		this.ancho = ancho;
		this.salto = false;
		this.saltoMaximo = false;
		this.princesa = Herramientas.cargarImagen("princesa.png");

	}

	public void subir() {
		this.y = this.y - 4;
	}

	public void caer() {
		this.y = this.y + 4;
	}

	public void moverIzquierda() {
		this.x = this.x - 1;
	}

	public void moverDerecha() {
		this.x = this.x + 1;
	}

	public Bola_Fuego disparar() {
		Bola_Fuego bola = new Bola_Fuego(this.x, this.y, 10, 10);
		return bola;
	}

	public void dibujar(Entorno e) {
		e.dibujarImagen(princesa, this.x - 5, this.y - 15, 0, 0.3);
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getAlto() {
		return alto;
	}

	public int getAncho() {
		return ancho;
	}

	public boolean getSalto() {
		return salto;
	}

	public boolean saltoTrue() {

		return this.salto = true;
	}
	
	public boolean saltoFalse() {

		return this.salto = false;
	}

	public boolean GetSaltoMaximo() {
		return saltoMaximo;
	}

	public boolean saltoMaximoTrue() {
		return this.saltoMaximo = true;
	}
	
	public boolean saltoMaximoFalse() {
		return this.saltoMaximo = false;
	}

	public void sonidoSalto () {
		Herramientas.play("salto.wav");;
	}
	
	public void saltoPrincesa() {
		
		
		if (this.getSalto() == true && this.getY() >= 250) {
			this.subir();
			
		}
		if (this.getY() <= 250) {
			this.saltoFalse();
			this.saltoMaximoTrue();

		}
		if (this.GetSaltoMaximo() == true && this.getY() < 500) {
			this.caer();
			if (this.getY() == 500) {
				this.saltoMaximoFalse();
			}
		}
	}
}