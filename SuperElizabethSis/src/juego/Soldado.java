package juego;

import java.awt.Image;

import entorno.Entorno;

import entorno.Herramientas;

public class Soldado {
	private double x;
	private int y;
	private int ancho;
	private int alto;
	private double velocidad;
	private Image soldado;

	Soldado(double x, int y, int ancho, int alto, double velocidad) {
		this.x = x;
		this.y = y;
		this.ancho = ancho;
		this.alto = alto;
		this.velocidad = velocidad;
		this.soldado = Herramientas.cargarImagen("soldado.png");
		
	}

	public void moverIzquierda() {
		this.x -= this.velocidad;
	}
	
	public void aumentarVelocidad(double n) {
		this.velocidad = n;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public int getAncho() {
		return ancho;
	}

	public int getAlto() {
		return alto;
	}

	public void sonidoSoldadoMuere () {
		Herramientas.play("SoldadoMuere.wav");
	}
	
	public void dibujar(Entorno e) {
		e.dibujarImagen(soldado, this.x + 20, this.y - 14, 0, 0.35);
	}
}