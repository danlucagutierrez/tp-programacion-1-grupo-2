package juego;

import java.awt.Image;

import entorno.Entorno;

import entorno.Herramientas;

public class Obstaculo {
	private double x;
	private int y;
	private int alto;
	private int ancho;
	private double velocidad;
	private Image tuboLargo;
	private Image tuboMediano;
	private Image tuboPequeno;
	private Image tubo80;

	Obstaculo(double x, int y, int alto, int ancho, double velocidad) {
		this.x = x;
		this.y = y;
		this.alto = alto;
		this.ancho = ancho;
		this.velocidad = velocidad;
		this.tuboLargo = Herramientas.cargarImagen("tubo_largo.png");
		this.tuboMediano = Herramientas.cargarImagen("tubo_mediano.png");
		this.tuboPequeno = Herramientas.cargarImagen("tubo peque�o.png");
		this.tubo80 = Herramientas.cargarImagen("tubo_80.png");
	}

	public void moverIzquierda() {
		this.x = this.x - this.velocidad;
	}

	public void modificarVelocidad(double n) {
		this.velocidad = n;
	}

	public void dibujarObs1(Entorno e) {
		e.dibujarImagen(tuboLargo, this.x, this.y, 0, 0.29);
	}

	public void dibujarObs2(Entorno e) {
		e.dibujarImagen(tuboMediano, this.x, this.y, 0, 0.3);
	}

	public void dibujarObs3(Entorno e) {
		e.dibujarImagen(tuboPequeno, this.x - 1, this.y, 0, 0.28);
	}

	public void dibujarObs4(Entorno e) {
		e.dibujarImagen(tubo80, this.x, this.y, 0, 0.33);
	}

	public double getX() {
		return x;
	}

	public int getAlto() {
		return alto;
	}

	public int getY() {
		return y;
	}

	public int getAncho() {
		return ancho;
	}

}